#include "module.h"
#include <algorithm>
#include <QDir>
#include <QErrorMessage>
#include <QDebug>

Module::Module( const QString &src, const QString &dest,
               QLabel *label, QFtp * ftp,
               QProgressBar *pgb,
               QObject *pobj) : QObject (pobj)
{
    m_op = None;
    m_ftp = ftp;
    m_src = src;
    m_pgb = pgb;
    m_dest = dest;
    m_label = label;

    connect(ftp, SIGNAL(commandStarted(int)), SLOT(ftpCommandStarted(int)));

    connect(m_ftp, SIGNAL(commandFinished(int,bool)),
               this, SLOT(ftpCommandFinished(int,bool)));
    connect(m_ftp, SIGNAL(listInfo(QUrlInfo)),
                this, SLOT(listInfo(QUrlInfo)));
}

Module::~Module()
{
    m_src = "";
    m_dest = "";
    m_ftp = nullptr;
    m_label = nullptr;
}

void Module::refreshTime()
{
    if (m_op != None)
        return;

    m_op = TimeUpdate;
    ftpCommandId = m_ftp->list(m_dest);
}

void Module::updateModule()
{
    if (m_op != None) //Если есть не законченная операция выходим
        return;
    if (!QFile::exists(m_src))
        return;
    m_list.erase(m_list.begin(), m_list.end());
    m_op = ModuleUpdate;
    QFileInfo file(m_dest);
    ftpCommandId = m_ftp->list(file.path());
}

void Module::MakeBackup()
{
    QFileInfo file(m_dest);
    QString path = file.path();
    std::sort(m_list.begin(), m_list.end(),
              [](const QUrlInfo &a, const QUrlInfo &b) {
                       return QUrlInfo::lessThan(a, b, QDir::Time);
                   }); //Сортируем список по времени

    auto lastFileIt = m_list.begin();
    for (auto it = m_list.begin(); it != m_list.end(); ++it)
       if (it->lastModified().date() == QDate::currentDate()){
            lastFileIt = it;
            --lastFileIt;
            break;
    }
    for (auto it = m_list.begin(); it != lastFileIt; ++it){  //Удаляем за предыдущий день
        m_ftp->remove(QString("%1/%2").arg(file.path()).arg(it->name()));
        qDebug()<<"deleted"<<it->name()<<it->lastModified();
        m_list.removeOne(*it);
    }

    QString today = QDate::currentDate().toString("dd.MM.yy"); //Переименновываем текущий модуль
    QString b_filename = QString("%1.%2_%3").arg(m_dest).arg(today).arg(m_list.size());
    ftpCommandId = m_ftp->rename(m_dest, b_filename);
}

void Module::UploadModule()
{
    QFile *file = new QFile(m_src);
    ftpCommandId = m_ftp->put(file, m_dest);
}

void Module::ftpCommandStarted(int commandId)
{
    if (commandId != ftpCommandId)
        return;
    auto command = m_ftp->currentCommand();
    if (command == QFtp::Put){
        connect(m_ftp, SIGNAL(dataTransferProgress(qint64,qint64)),
                    this, SLOT(updateDataTransferProgress(qint64,qint64)));
    }
}

void Module::ftpCommandFinished(int commandId, bool error)
{
    if (commandId != ftpCommandId)
        return;

    auto command = m_ftp->currentCommand();

    if (error && (command != QFtp::Rename)){
        m_op = None;
        return;
    }

    if (command == QFtp::List) {
        if (m_op == TimeUpdate)
            m_op = None;
        else if (m_op == ModuleUpdate) {
            MakeBackup();
            m_op = None;
        }
    }
    else if (command == QFtp::Rename) {
        UploadModule();
    }
    else if (command == QFtp::Put) {
        disconnect(m_ftp, SIGNAL(dataTransferProgress(qint64,qint64)),
                    this, SLOT(updateDataTransferProgress(qint64,qint64)));
        m_op = None;
    }
}

void Module::listInfo(const QUrlInfo &urlInfo)
{
    if (m_ftp->currentId() != ftpCommandId) //Если команда не от этого класса
        return;

    switch (m_op) {
        case TimeUpdate:
        {
            QDateTime date = urlInfo.lastModified();
            date = date.addSecs(10800);
            m_label->setText(date.toString());
            return;
        }
        case ModuleUpdate:
            m_list.append(urlInfo);
            return;
        default:
            return;
    }
}

void Module::updateDataTransferProgress(qint64 readBytes,
                                        qint64 totalBytes)
{
    m_pgb->setMaximum(static_cast<int>(totalBytes));
    m_pgb->setValue(static_cast<int>(readBytes));
}
