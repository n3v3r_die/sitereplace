#ifndef MODULE_H
#define MODULE_H
#include <QLabel>
#include <QProgressBar>
#include <QFtp>

 enum Operation{None, TimeUpdate, ModuleUpdate};

class Module : public QObject
{
    Q_OBJECT
public:
    Module(const QString &src, const QString &dest,
           QLabel *label = nullptr,
           QFtp *ftp = nullptr,
           QProgressBar *pgb = nullptr,
           QObject *pobj = nullptr);
     ~Module();
    Operation op();
    void refreshTime();
    void updateModule();
private:
    int ftpCommandId;
    QFtp * m_ftp;
    Operation m_op;
    QLabel * m_label;
    QProgressBar *m_pgb;
    QString m_src, m_dest;
    QList <QUrlInfo> m_list;
    void MakeBackup();
    void UploadModule();

private slots:
    void ftpCommandStarted(int commandId);
    void ftpCommandFinished(int commandId, bool error);
    void listInfo(const QUrlInfo &urlInfo);
    void updateDataTransferProgress(qint64 readBytes, qint64 totalBytes);
};

#endif // MODULE_H
