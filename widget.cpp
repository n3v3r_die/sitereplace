#include "widget.h"
#include "ui_widget.h"
#include <algorithm>
#include <QErrorMessage>
#include <QDebug>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ftp = new QFtp(this);
    ftp->setTransferMode(QFtp::Passive);
    connect(ftp, SIGNAL(commandFinished(int,bool)),
               this, SLOT(ftpCommandFinished(int,bool)));
    ftp->connectToHost("intra.net");
    ftp->login("evgeniy", "1234qwer");
    QList<QProgressBar*> allProgressBar = this->findChildren<QProgressBar *>();
    for (auto i = allProgressBar.begin(); i != allProgressBar.end(); ++i) {
        static_cast<QProgressBar *>(*i)->setValue(0);
    }
    modules["CEH"] = new Module("Z:/Общий_программист/__ для сайта/delphi6_цех_модуль/asptd.exe",
                                "temp/asptd.exe", ui->labelCEH, ftp,
                                ui->pbCEH, this);
    modules["OGT"] = new Module("Z:/Общий_программист/__ для сайта/delphi6_огт_модуль/asptd.exe",
                                "temp/asptd.exe", ui->labelOGT, ftp,
                                ui->pbOGT,this);
    modules["CKS"] = new Module( "Z:/Общий_программист/__ для сайта/delphi6_cks_модуль/asptd.exe",
                                "temp/asptd.exe", ui->labelCKS, ftp,
                                 ui->pbCKS, this);
    modules["KPP"] = new Module("Z:/Общий_программист/__ для сайта/delphi6_цех_кпп/asptd_kpp.exe",
                                "temp/asptd_kpp.exe", ui->labelKPP, ftp,
                                ui->pbKPP, this);
    modules["BUHG"] = new Module("Z:/Общий_программист/__ для сайта/delphiXE_ANTON_PROJECT/asptd_buhg.exe",
                                "temp/asptd_buhg.exe", ui->labelBUHG, ftp,
                                 ui->pbBUHG, this);
    modules["OVIT"] = new Module("Z:/Общий_программист/__ для сайта/delphiXE_ANTON_PROJECT/asptd_ovit.exe",
                                "temp/asptd_ovit.exe", ui->labelOVIT, ftp,
                                 ui->pbOVIT, this);
    modules["UP"] = new Module("Z:/Общий_программист/__ для сайта/delphiXE_КПП_УП/antonUP.exe",
                                "temp/antonUP.exe", ui->labelUP, ftp,
                                 ui->pbUP, this);
    modules["Gantt"] = new Module("Z:/Общий_программист/__ для сайта/delphiXE_GanttProject/GanttProject.exe",
                                "temp/GanttProject.exe", ui->labelGantt, ftp,
                                 ui->pbGantt, this);
    on_pushButtonTime_clicked();
}

Widget::~Widget()
{
    delete ui;
}


void Widget::ftpCommandFinished(int commandId, bool error){
    auto command = ftp->currentCommand();
    if (error){
        if (command == QFtp::Rename)
            (new QErrorMessage)->showMessage("Не удалось сохранить резервную копию модуля");
        else
            (new QErrorMessage)->showMessage(
                    ftp->errorString() + QString::number(commandId)
                    );
        return;
    }
    switch (command) {
    case QFtp::ConnectToHost:
        qDebug()<<"Connect to host success";
        return;
    case QFtp::Login:
        qDebug()<<"Login success";
        return;
    default:
        return;
    }
}


void Widget::on_pushButtonTime_clicked()
{
    modules["CEH"]->refreshTime();
    modules["OGT"]->refreshTime();
    modules["CKS"]->refreshTime();
    modules["KPP"]->refreshTime();
    modules["BUHG"]->refreshTime();
    modules["OVIT"]->refreshTime();
    modules["UP"]->refreshTime();
    modules["Gantt"]->refreshTime();
}

void Widget::on_pushButtonUpgrade_clicked()
{
    if (ui->cbCEH->isChecked())
        modules["CEH"]->updateModule();
    if (ui->cbOGT->isChecked())
        modules["OGT"]->updateModule();
    if (ui->cbCKS->isChecked())
        modules["CKS"]->updateModule();
    if (ui->cbKPP->isChecked())
        modules["KPP"]->updateModule();
    if (ui->cbBUHG->isChecked())
        modules["BUHG"]->updateModule();
    if (ui->cbOVIT->isChecked())
        modules["OVIT"]->updateModule();
    if (ui->cbUP->isChecked())
        modules["UP"]->updateModule();
    if (ui->cbGantt->isChecked())
        modules["Gantt"]->updateModule();
}
