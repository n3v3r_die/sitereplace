#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtFtp>
#include <QLabel>
#include <module.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;
    QFtp * ftp;
    QString name;
    QMap <int, QLabel*> m_labels;
    QMap <int, QList <QUrlInfo>> m_listUrl;
    QMap <QString, Module *> modules;

private slots:
    void ftpCommandFinished(int commandId, bool error);
    void on_pushButtonTime_clicked();
    void on_pushButtonUpgrade_clicked();
};

#endif // WIDGET_H
